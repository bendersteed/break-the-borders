document.addEventListener('turbolinks:load', function() {
   var elem = document.querySelector('#modal-contact');
   var instance = new M.Modal(elem, {});
});

document.addEventListener('turbolinks:before-cache', function() {
    elem = document.querySelector('#modal-contact');
    instance = M.Modal.getInstance(elem);
    if (instance){
	instance.destroy();
    }
});

document.addEventListener('turbolinks:load', function() {
    var el = document.querySelectorAll('.tabs');
    var instance = M.Tabs.init(el, {});
    var elems = document.querySelectorAll('.datepicker');
    var instances = M.Datepicker.init(elems, {});
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems, { hover: false, coverTrigger: false });
});

document.addEventListener('turbolinks:load', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {});
});

document.addEventListener('turbolinks:load', function() {
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems, {});
});

