class Article < ApplicationRecord
  has_many :applications
  has_many :users, through: :applications
  has_many_attached :application_documents
  default_scope -> { order(created_at: :desc) }
  validates :title, presence: true, length: {maximum: 80}, uniqueness: true
  validates :summary, presence: true, length: {maximum: 80}
  validates :content, presence: true
  validates :deadline, presence: true, if: :project
    
  def previous_article
    Article.where(["id < ?", id]).first
  end
  
  def next_article
    Article.where(["id > ?", id]).last
  end
end
