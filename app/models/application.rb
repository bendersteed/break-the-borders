class Application < ApplicationRecord
  belongs_to :user
  belongs_to :article
  has_one_attached :application_document
  validates :user_id, presence: true
  validates :article_id, presence: true
  validates :message, presence: true
  # validate if article is project!
  
end
