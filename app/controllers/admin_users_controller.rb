class AdminUsersController < AdminController
  include SessionsHelper

  def index
    @users = User.paginate(:page => params[:page], :per_page => 10)
    store_location
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = 'User deleted'
    redirect_back_or root_path
  end

  def admin
    user = User.find(params[:id])
    if user.admin
      user.update_attribute(:admin, FALSE)
      flash[:success] = 'User has been denied superpowers'
    else
      user.update_attribute(:admin, TRUE)
      flash[:success] = 'User has been granted superpowers'
    end
    redirect_to admin_users_path
  end
end
