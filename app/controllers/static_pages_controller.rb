class StaticPagesController < ApplicationController
  def home
    @latest_articles = Article.limit(5).order('id desc')
  end

  def about; end

end
