class ArticlesController < ApplicationController
  include SessionsHelper
  after_action :remove_application_errors, only: :show
  
  def show
    @article = Article.find(params[:id])
    @application = current_user.applications.build if logged_in?
    store_location
  end

  def index
    @articles = Article.paginate(:page => params[:page], :per_page => 10)
  end

  private

  def user_params
    params.require(:article).permit(:title, :summary, :content,
                                    :project, :deadline, application_documents: [] )
  end

  def remove_application_errors
     session[:applicationerrors] = []
  end
end
