class AdminArticlesController < AdminController
  def index
    @articles = Article.paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @article = Article.new
  end


  def create
    @article = Article.new(user_params)
    if @article.save
      flash[:success] = "The article has been successfully added!"
      redirect_to @article
    else
      flash.now[:failure] = "Something is wrong"
      render 'new'
    end
  end

  def update
    @article = Article.find(params[:id])
    if @article.update_attributes(user_params)
      flash[:success] = "The article has been successfully updated!"
      redirect_to @article
    else
      flash[:failure] = "Something is wrong"
      render 'edit'
    end
  end

  def destroy
    Article.find(params[:id]).destroy
    flash[:success] = "Article deleted"
    redirect_to admin_articles_path
  end

  def edit
    @article = Article.find(params[:id])
  end

   private

  def user_params
    params.require(:article).permit(:title, :summary, :content,
                                    :project, :deadline, application_documents: [] )
  end
end
