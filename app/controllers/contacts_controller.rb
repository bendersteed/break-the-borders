class ContactsController < ApplicationController
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new contact_params
    if @contact.valid?
      ContactMailer.contact(@contact).deliver_now
      redirect_to contact_path
      flash[:success] = "Your message has been sent! We will be in touch soon!"
    else
      flash[:warning] = "There was an error sending your message. Please try again."
      render :new
    end
  end

  private
  def contact_params
    params.require(:contact).permit(:name, :email,:subject, :message)
  end

end
