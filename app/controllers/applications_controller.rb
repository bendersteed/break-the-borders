class ApplicationsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]

  def create
    @application = current_user.applications.build(application_params)
    if @application.save
      flash[:success] = "Your application has been submitted!"
      session[:applicationerrors] = []
    else
      session[:applicationerrors] = @application.errors.full_messages
      flash[:failure] = "There was some problem!"
    end
    redirect_back_or root_url
  end

  def destroy
    @application = Application.find(params[:id])
    if @application.progress == "pending"
      @application.destroy
      flash[:success] = "Your application has been deleted"
    else
      flash[:failure] = "This is not permitted!"
    end
    redirect_to current_user
  end

  private
  
  def application_params
    params.require(:application).permit(:article_id, :message, :application_document)
  end
end
