class AdminApplicationsController < AdminController
  def index
    @articles = Article.where(project: true).paginate(:page => params[:page],
                                                      :per_page => 10)
    store_location
  end

   def destroy
     Application.find(params[:id]).destroy
    flash[:success] = 'Application deleted'
    redirect_back_or root_path
  end

  def accept
    application = Application.find(params[:id])
    application.update_attribute(:progress, "accepted")
    flash[:success] = 'Application has been accepted'
    redirect_back_or root_path
  end

  def reject
    application = Application.find(params[:id])
    application.update_attribute(:progress, "rejected")
    flash[:success] = 'Application has been rejected'
    redirect_back_or root_path
  end
end
