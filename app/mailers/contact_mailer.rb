class ContactMailer < ApplicationMailer
  def contact(contact)
    @contact = contact
    mail to: 'bendersteed@teknik.io', subject: contact.subject
  end
    
end
