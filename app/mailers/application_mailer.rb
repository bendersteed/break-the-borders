class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@pacific-thicket-98962.herokuapp.com'
  layout 'mailer'
end
