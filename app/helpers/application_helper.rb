module ApplicationHelper
 
  # Return the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = 'Break the Borders'
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  # Set active class to current page's li in navbar
  def active?(link_path)
    current_page?(link_path) ? "active" : ""
  end  
  
end
