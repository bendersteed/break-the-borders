require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  def setup
    @article = Article.new(title: "A fine article",
                           summary: "This is the stuff that shows on front-page",
                           content: "Not much to say for now")
  end

  test "should be valid" do
    assert @article.valid?
  end

  test "title should be present" do
    @article.title = ""
    assert_not @article.valid?
  end

  test "summary should be present" do
    @article.summary = ""
    assert_not @article.valid?
  end

  test "content should be present" do
    @article.content = ""
    assert_not @article.valid?
  end


  
end
