Rails.application.routes.draw do
  root 'static_pages#home'
  get '/about', to: 'static_pages#about'
  get '/news', to: 'articles#index'
  get '/contact', to: 'contacts#new'
  get '/signup', to: 'users#new'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  resources :users, only: %i[show index update create]
  resources :articles, only: %i[show index]
  resources :account_activations, only: %i[edit]
  resources :password_resets, only: %i[new create edit update]
  resources :applications, only: %i[create destroy]
  resources :admin, only: %i[index]
  resources :admin_users, only: %i[index destroy]
  resources :admin_articles, only: %i[index create update destroy new edit]
  resources :admin_applications, only: %i[index destroy]
  resources :contacts, only: %i[create]

  put 'admin_users/:id', to: 'admin_users#admin'
  put 'admin_applications/:id', to: 'admin_applications#accept'
  post 'admin_applications/:id', to: 'admin_applications#reject'
end
