class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :title
      t.string :summary
      t.text :content
      t.boolean :project
      t.timestamp :deadline

      t.timestamps
    end
  end
end
