class AddMessageToApplication < ActiveRecord::Migration[5.2]
  def change
    add_column :applications, :message, :text
  end
end
