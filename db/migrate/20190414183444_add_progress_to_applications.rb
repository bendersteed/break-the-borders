class AddProgressToApplications < ActiveRecord::Migration[5.2]
  def change
    add_column :applications, :progress, :string, :default => 'pending'
  end
end
